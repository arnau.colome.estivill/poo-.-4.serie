package dto;

public class Serie {

	private String titulo;
	private int ntemporadas;
	private Boolean entregado;
	private String genero;
	private String creador;
	
	public Serie() {
		this.titulo = "";
		this.ntemporadas = 3;
		this.entregado = false;
		this.genero = "";
		this.creador = "";
	}
	
	
	public Serie(String titulo, String creador) {
		this.titulo = titulo;
		this.creador = creador;
	}

	
	public Serie(String titulo, int ntemporadas, String genero, String creador) {
		this.titulo = titulo;
		this.ntemporadas = ntemporadas;
		this.genero = genero;
		this.creador = creador;
	}
	
}

# POO . 4.Serie

Ejercicio 4

4. Crearemos una clase llamada Serie con las siguientes características:
• Sus atributos son titulo, numero de temporadas, entregado, genero y creador.
• Por defecto, el numero de temporadas es de 3 temporadas y entregado false. El
resto de atributos serán valores por defecto según el tipo del atributo.
• Los constructores que se implementaran serán:
o Un constructor por defecto.
o Un constructor con el titulo y creador. El resto por defecto.
o Un constructor con todos los atributos, excepto de entregado.